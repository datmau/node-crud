
# BASIC CRUD
REST API básica con un Crud basado en la entidad usuario.

## Requerimientos
- Node 18 or latest
- Postgres 15

## Inicializar el proyecto

``` bash
    npm install
```


## Licence and deployment Status

#### Deployed on Render

[![MIT License](https://img.shields.io/badge/License-MIT-blue.svg)](https://choosealicense.com/licenses/mit/)


[![Build](https://img.shields.io/badge/Build-Live-green)](https://choosealicense.com/licenses/mit/)


## Deployment

Para correr este proyecto en modo desarrollador:

```bash
  npm run dev
```

Para correr este proyecto:

```bash
  npm run start
```


## Variables de entorno

Para correr este proyecto necesita las siguientes Variables de entorno en un arrchivo .env en la raíz.

`DB_HOST`
`DB_USERNAME`
`DB_PASSWORD`
`DB_DATABASE`
`DB_DAILECT`
`DB_PORT`
`PORT`
`ACCESS_TOKEN_SECRET`
`REFRESH_TOKEN_SECRET`
`ACCESS_TOKEN_EXPIRY`
`REFRESH_TOKEN_EXPIRY`
`REFRESH_TOKEN_COOKIE_NAME`

