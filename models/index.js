require("dotenv").config();

const { DB_HOST, DB_USERNAME, DB_PASSWORD, DB_DATABASE, DB_DAILECT, DB_PORT } =
  process.env;
const { Sequelize, DataTypes } = require("sequelize");

const sequelize = new Sequelize({
  host: DB_HOST,
  username: DB_USERNAME,
  password: DB_PASSWORD,
  database: DB_DATABASE,
  dialect: DB_DAILECT,
  port: DB_PORT,
});

sequelize
  .authenticate()
  .then(() => {
    console.log(`Se estableció conexión con la base de datos.`);
  })
  .catch((err) => {
    console.log("No se peude conectar con la base de datos:", err);
  });

const db = {};
db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.users = require("./user.model")(sequelize, DataTypes);

module.exports = db;
