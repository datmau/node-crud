const jwt = require("jsonwebtoken");
const db = require("../models");
require("dotenv").config();
const Sequelize = require("sequelize");
const Op = Sequelize.Op;

const {
  ACCESS_TOKEN_SECRET,
  ACCESS_TOKEN_EXPIRY,
  REFRESH_TOKEN_SECRET,
  REFRESH_TOKEN_EXPIRY,
} = process.env;

const User = db.users;

const login = async (req, res) => {
  try {
    const { email, password } = req.body;

    const user = await User.findOne({ where: { email: email } });

    if (user) {
      const isPasswordValid = password == user.password ? true : false;
      if (isPasswordValid) {
        const accessToken = jwt.sign(
          { userName: user.userName, email: user.email },
          ACCESS_TOKEN_SECRET,
          { expiresIn: ACCESS_TOKEN_EXPIRY }
        );
        const refreshToken = jwt.sign(
          { userName: user.userName, email: user.email },
          REFRESH_TOKEN_SECRET,
          { expiresIn: REFRESH_TOKEN_EXPIRY }
        );
        res.status(200).send({
          token: accessToken,
          refreshToken: refreshToken,
        });
      } else {
        return res.status(401).send("Authentication failed");
      }
    } else {
      return res.status(401).send("Authentication failed");
    }
  } catch (error) {
    console.log("Error on login: ", error);
  }
};

const createUser = async (req, res) => {
  try {
    const { userName, email, password } = req.body;
    const data = {
      userName,
      email,
      password,
    };

    const user = await User.create(data);

    if (user) {
      let token = jwt.sign(
        { name: user.username, password: user.password, email: user.email },
        REFRESH_TOKEN_SECRET,
        { expiresIn: REFRESH_TOKEN_EXPIRY }
      );
      res.cookie("jwt", token, { maxAge: 1 * 24 * 60 * 60, httpOnly: true });
      return res.status(200).send(user);
    } else {
      return res.status(400).send("Invalid request body");
    }
  } catch (error) {
    console.log("Error on sign up", error);
  }
};

const getUsers = async (req, res) => {
  var queryType = req.query.query;
  try {
    const users = await User.findAll({
      attributes: { exclude: ["password"] },
    });
    if (users) {
      return res.status(200).json(users);
    } else {
      return res.status(400).send("Invalid request body");
    }
  } catch (error) {
    console.log("getUser - queryType:", queryType, " - [Error]: ", error);
  }
};

const getUserById = async (req, res) => {
    console.log(req.params)
    const {id:userId} = req.params;
  try {
    const user = await User.findOne({
      where: {
       id: userId
      },
      attributes: { exclude: ["password"] },
    });
    if (user) {
      return res.status(200).json(user);
    } else {
      return res.status(400).send("Invalid request body");
    }
  } catch (error) {}
};

const updateUser = async (req, res) => {
  var userId = req.params.id;
  console.log("updateUser - updateItem: ", userId);
  const { userName, email, password } = req.body;
  try {
    const user = await User.findOne({
      where: {
        id: userId,
      },
    });

    if (!user) {
      return res.status(409).send("Requested " + userId + " wasn't found!");
    }
    const checkSameUser = await User.findOne({
      where: {
        email: email,
      },
    });

    if (checkSameUser &&  checkSameUser.email != user.email) {
      return res
        .status(403)
        .send(
          "El correo " + email + " pertenece a otro usuario cambie, e intente de nuevo."
        );
    }

    await User.update(
      {
        userName: userName,
        email: email,
        password: password,
      },
      {
        where: { id: userId },
      }
    );

    const findUser = await User.findOne({
      where: {
        id: userId,
      },
      attributes: { exclude: ["password"] },
    });

    return res.status(200).send(findUser);
  } catch (error) {
    console.log("Error on update:", updateItem, "with message: ", error);
  }
};

const deleteUser = async (req, res) => {
  const userId = req.params.id;
  try {
    const user = await User.findOne({
      where: { id: userId },
    });
    if (!user) {
      return res.status(409).send("Requested " + userId + " wasn't found!");
    } else {
      await user.destroy();
      return res.status(200).send("OK");
    }
  } catch (error) {
    console.log("error on delete user:", email, "with message", error);
  }
};

module.exports = {
  login,
  createUser,
  getUsers,
  getUserById,
  updateUser,
  deleteUser,
};
