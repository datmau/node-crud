const express = require('express')
const userController = require('../controllers/user.controller')
const {login, createUser, getUsers, getUserById, updateUser, deleteUser } = userController
const userAuth = require('../middlewares/userAuth')

const router = express.Router()

router.post('/users', userAuth.saveUser, createUser)

router.post('/login', login)

router.get('/users', getUsers)
router.get('/users/:id', getUserById)
router.put('/users/:id', updateUser)
router.delete('/users/:id', deleteUser)

module.exports = router