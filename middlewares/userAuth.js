const db = require("../models");
const User = db.users;

const saveUser = async (req, res, next) => {
    try {
        const emailcheck = await User.findOne({
            where: {
                email: req.body.email,
            },
        });
        if (emailcheck) {
            return res.status(403).send("Email is duplicate. You don't have permission to perform this operation!");
        }
        const username = await User.findOne({
            where: {
                userName: req.body.userName,
            },
        });
        if (username) {
            return res.status(409).send("username already token");
        }
        next();
    } catch (error) {
        console.log(error);
    }
};

module.exports = {
 saveUser,
};