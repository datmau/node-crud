const express = require('express')
const { createServer } = require("http");
const cookieParser = require('cookie-parser')
const db = require('./models')
const userRoutes = require ('./routes/user.routes')
const ip = require('ip');
const host = ip.address();
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');
const fs = require('fs')
var options = {
  customCss: fs.readFileSync(("./swagger.css"), 'utf8')
};

const port = process.env.PORT || 3000

const app = express()

app.use(express.json())
app.use('/swagger-ui', swaggerUi.serve, swaggerUi.setup(swaggerDocument, options));
app.use(express.urlencoded({ extended: true }))
app.use(cookieParser())


db.sequelize.sync().then(() => {
  console.log("La base de datos se sincronizó.")
})
app.use(function (req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader(
    "Access-Control-Allow-Headers",
    "X-Requested-With, x-access-token, Origin, Content-Type, Accept"
  );
  res.setHeader('Access-Control-Allow-Credentials', true);
  next();
});

app.use('/api', userRoutes)
const server = createServer(app);

server.listen(port, function (error) {
  if (error) console.log("Error en inicio servidor.");
  console.log(`Server listo en http://${host}:${port}`);
})

app.on('close', function(){
  db.sequelize.close();
})